import sys
import os


ruta_actual = os.path.abspath(os.path.dirname("ChallengeController.py"))
sys.path.append(os.path.join(ruta_actual, "com/challenge/service"))

from UsuariosService import leer_usuarios, agregar, eliminar, consultar
from fastapi import FastAPI

controlador = FastAPI()


@controlador.get('/', tags=['Inicio'])
def inicio():
    """
    Validar inicio.
    """
    return {"Bienvenido": "API Rest de Usuarios de Diego"}


@controlador.get('/usuario', tags=['Todos los Usuarios'])
def listar_usuarios():
    """
    Consultar todos los usuarios disponibles.
    """
    return leer_usuarios()


@controlador.post('/agregar', tags=['Nuevo Usuario'])
def agregar_usuario(nombre, rol):
    """
    Crear usuario.

    - *Nombre*: Nombre del usuario a agregar.
    - *Rol*: Rol para el usuario, considere (admin, films, people, locations, species, vehicles.
    """
    return agregar(nombre, rol)


@controlador.delete('/eliminar', tags=['Borra Usuario'])
def eliminar_usuario(nombre):
    """
    Eliminar usuario.

    - *Nombre*: Nombre del usuario a eliminar.
    """
    return eliminar(nombre)


@controlador.get('/usuario/{nombre}', tags=['Usuario por Nombre'])
def obtener_usuario(name: str):
    """
    Buscar usuario por nombre.

    - *nombre*: Nombre del usuario a agregar.
    """
    return consultar(name)
