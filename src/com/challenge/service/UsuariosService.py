import sys
import os
import requests

ruta_actual = os.path.abspath(os.path.dirname("UsuariosService.py"))
sys.path.append(os.path.join(ruta_actual, "com/challenge/models"))


from Modelos import Usuario


usuarios = set()
urlBase = "https://ghibliapi.vercel.app/"


# Metodo para listar todos los usuarios, si el conjunto esta vacio
# se retorna un mensaje, caso contrario se muestra la lista
def leer_usuarios():
    if not usuarios:
        return ["No hay registros"]
    else:
        for usuario in usuarios.copy():
            obtenerInformacion(usuario)
        return usuarios


# Metodo para agrega un usuario nuevo, se valida que el usuario no exista
# si el nuevo usuario tiene rol distinto sera agregado
def agregar(nombre, rol):
    existe = False
    for usuario in usuarios.copy():
        if nombre == usuario.nombre and rol == usuario.rol:
            existe = True
    if not existe:
        usuarios.add(Usuario(nombre, rol))
        return "Usuario agregado"
    else:
        return "Usuario ya existe"


# Metodo para eliminar un usuario mediante su nombre
# se eliminara el primer usuario que se encuentre en la lista
def eliminar(nombre):
    try:
        for usuario in usuarios.copy():
            if nombre == usuario.nombre:
                usuarios.remove(usuario)
                return "Usuario eliminado"
        return "Usuario no existe"
    except (ValueError, KeyError):
        return "Usuario no existe"


# Metodo para consultar un usuario mediante su nombre
# se valida que el usuario exista
def consultar(nombre):
    for usuario in usuarios.copy():
        if usuario.nombre == nombre:
            obtenerInformacion(usuario)
            return usuario
    return "Usuario no existe"


# Metodo para obtener la informacion de Studio Ghibli
# de acuerdo con el rol del usuario
def obtenerInformacion(usuario):
    usuario.ghibli = (requests.get(urlBase + usuario.rol)).json()

